import data.Data;
import solution.SolutionGenerator;

import java.io.*;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choix = 0;

        do {
            afficherMenu();
            choix = scanner.nextInt();
            if (choix >= 1 && choix <= 3) {
                System.out.println("Quel fichier voulez-vous utiliser ?");
                System.out.println("Entrez le numéro du fichier (ex : 101 pour data101.vrp) : ");
                String number = scanner.next();
                String nomFichier = "data/data" + number + ".vrp";
                File file = new File(nomFichier);

                if (file.exists()) {
                    // Paramètres Recuit Simulé \\
                    double temperatureInitiale = 20;
                    double decreaseFactor = 0.97;
                    int nbIterationsSA = 80;

                    // Paramètres Recherche Tabou \\
                    int nbIterationsTabu = 1500;
                    int tabuListSize = 150;

                    Data data = new Data(nomFichier);
                    long startTime, endTime, duration;
                    double durationSeconds;

                    SolutionGenerator solutionGenerator = new SolutionGenerator(data);
                    solutionGenerator.generateRandomSolution();
                    switch (choix) {
                        case 1:
                            System.out.println("Vous avez choisi Tabu Search sur le fichier " + nomFichier + ".");
                            System.out.println("Paramètres : Nombre d'itérations = " + nbIterationsTabu + ", Taille de la liste tabou = " + tabuListSize + ".");
                            System.out.println("Avec affichage graphique.");
                            System.out.println();

                            startTime = System.nanoTime();
                            solutionGenerator.tabuSearch(nbIterationsTabu, tabuListSize, true);
                            endTime = System.nanoTime();
                            duration = (endTime - startTime);
                            durationSeconds = (double) duration / 1_000_000_000.0;
                            System.out.println("Fin de Tabu Search : Executé en " + durationSeconds + " secondes.");
                            break;
                        case 2:
                            System.out.println("Vous avez choisi Recuit Simulé sur le fichier " + nomFichier + ".");
                            System.out.println("Paramètres : Température initiale = " + temperatureInitiale + ", Facteur de décroissance = " + decreaseFactor + ", Nombre d'itérations par température = " + nbIterationsSA + ".");
                            System.out.println("Avec affichage graphique.");
                            System.out.println();

                            startTime = System.nanoTime();
                            solutionGenerator.simulatedAnnealing(temperatureInitiale, decreaseFactor, nbIterationsSA, true);
                            endTime = System.nanoTime();
                            duration = (endTime - startTime);
                            durationSeconds = (double) duration / 1_000_000_000.0;
                            System.out.println("Fin de Recuit simulé : Executé en " + durationSeconds + " secondes.");
                            break;
                        case 3:
                            System.out.println("Vous avez choisi de comparer les deux métaheuristiques sur le fichier " + nomFichier + ".");
                            System.out.println("Paramètres Recuit simulé : Température initiale = " + temperatureInitiale + ", Facteur de décroissance = " + decreaseFactor + ", Nombre d'itérations par température = " + nbIterationsSA + ".");
                            System.out.println("Paramètres Tabu Search : Nombre d'itérations = " + nbIterationsTabu + ", Taille de la liste tabou = " + tabuListSize + ".");
                            System.out.println("Sans affichage graphique.");

                            // Recuit simulé
                            startTime = System.nanoTime();
                            solutionGenerator.simulatedAnnealing(temperatureInitiale, decreaseFactor, nbIterationsSA, false);
                            endTime = System.nanoTime();
                            duration = (endTime - startTime);
                            durationSeconds = (double) duration / 1_000_000_000.0;
                            int durationRecuit = (int) durationSeconds;
                            double resultatRecuit = solutionGenerator.getNeighbor().getFitness();

                            // Tabu Search
                            solutionGenerator = new SolutionGenerator(data);
                            solutionGenerator.generateRandomSolution();
                            startTime = System.nanoTime();
                            solutionGenerator.tabuSearch(nbIterationsTabu, tabuListSize, false);
                            endTime = System.nanoTime();
                            duration = (endTime - startTime);
                            durationSeconds = (double) duration / 1_000_000_000.0;
                            int durationTabu = (int) durationSeconds;
                            double resultatTabu = solutionGenerator.getNeighbor().getFitness();

                            // Affichage des résultats
                            System.out.println();
                            System.out.println("Fin des deux algorithmes.");
                            System.out.println("Recuit simulé a trouvé une fitness de " + resultatRecuit + " en " + durationRecuit + " secondes.");
                            System.out.println("Tabu Search a trouvé une fitness de " + resultatTabu + " en " + durationTabu + " secondes.");
                            System.out.println();
                            break;
                    }
                } else {
                    System.out.println("/!\\ Le fichier " + nomFichier + " n'existe pas.");
                    choix = 0;
                }

            } else if (choix == 4) {
                System.out.println("Au revoir !");
            } else {
                System.out.println("Choix invalide.");
            }
        } while (choix != 4);
    }

    public static void afficherMenu() {
        System.out.println("===== Projet VRPTW =====");
        System.out.println("Quelle métaheuristique voulez-vous utiliser ?");
        System.out.println("1. Tabu Search");
        System.out.println("2. Recuit simulé");
        System.out.println("3. Comparer les deux");
        System.out.println("4. Quitter");
        System.out.print("Choisissez une option : ");
    }
}