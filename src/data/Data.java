package data;

import structure.Client;

import java.io.*;
import java.util.LinkedList;

/**
 * Classe permettant le recueil des données contenues dans les fichiers
 */

public class Data {
    /**
     * Nom du fichier parsé
     */
    private String filename;
    /**
     * Nombre total de clients totaux
     */
    private int nbClients;
    /**
     * Contenance maximum des vehicules
     */
    private int maxQuantity;
    /**
     * On stocke le dépot sous la forme d'un client
     */
    private Client deposit;
    /**
     * On stocke la totalité des clients sous la forme d'une hashmap avec comme clé leur identifiant
     */
    private LinkedList<Client> clients;

    public Data(String filename) {
        this.filename = filename;
        this.deposit = new Client();
        this.clients = new LinkedList<>();
        this.getContent();
    }

    public void getContent(){
        File file = new File(this.filename);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            reader.readLine();
            reader.readLine();
            reader.readLine();
            reader.readLine();
            reader.readLine();
            String line = reader.readLine();
            this.nbClients = Integer.parseInt(line.split(" ")[1]);
            line = reader.readLine();
            this.maxQuantity = Integer.parseInt(line.split(" ")[1]);
            reader.readLine();
            reader.readLine();
            line = reader.readLine();
            String[] temp = line.split(" ");
            deposit.setIdName(0);
            deposit.setX(Integer.parseInt(temp[1]));
            deposit.setY(Integer.parseInt(temp[2]));
            deposit.setReadyTime(Integer.parseInt(temp[3]));
            deposit.setDueTime(Integer.parseInt(temp[4]));
            reader.readLine();
            reader.readLine();
            line = reader.readLine();
            while(line != null){
                temp = line.split(" ");
                Client client = new Client();
                client.setIdName(Integer.parseInt(temp[0].substring(1)));
                client.setX(Integer.parseInt(temp[1]));
                client.setY(Integer.parseInt(temp[2]));
                client.setReadyTime(Integer.parseInt(temp[3]));
                client.setDueTime(Integer.parseInt(temp[4]));
                client.setDemand(Integer.parseInt(temp[5]));
                client.setService(Integer.parseInt(temp[6]));
                clients.add(client);
                line = reader.readLine();
            }

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public double calculateMininumVehicules() {
        double sum = 0;
        for(Client client : clients){
            sum += client.getDemand();
        }
        return sum / maxQuantity;
    }

    public String getFilename() {
        return filename;
    }

    public int getNbClients() {
        return nbClients;
    }

    public int getMaxQuantity() {
        return maxQuantity;
    }

    public Client getDeposit() {
        return deposit;
    }

    public LinkedList<Client> getClients() {
        return clients;
    }

}
