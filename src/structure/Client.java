package structure;

/**
 * Le client représente un point du graphe
 */
public class Client {
    /**
     * Id et nom du client
     */
    private int idName;
    /**
     * Position x du client
     */
    private int x;
    /**
     * Position y du client
     */
    private int y;
    /**
     * Temps (en ua) à partir duquel le client est prêt à recevoir son colis
     */
    private double readyTime;
    /**
     * Temps (en ua) à partir duquel le client ne peut plus recevoir le colis
     */
    private double dueTime;
    /**
     * Taille du colis
     */
    private int demand;
    /**
     * Temps que prend la livraison (10 pour tout client normalement)
     */
    private int service;

    private boolean visited;

    public Client() {
        this.idName = 0;
        this.x = 0;
        this.y = 0;
        this.readyTime = 0;
        this.dueTime = 0;
        this.demand = 0;
        this.service = 0;
        this.visited = false;
    }

    public Client(int idName, int x, int y, double readyTime, double dueTime, int demand, int service) {
        this.idName = idName;
        this.x = x;
        this.y = y;
        this.readyTime = readyTime;
        this.dueTime = dueTime;
        this.demand = demand;
        this.service = service;
    }

    public int getIdName() {
        return idName;
    }

    public void setIdName(int idName) {
        this.idName = idName;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getReadyTime() {
        return readyTime;
    }

    public void setReadyTime(double readyTime) {
        this.readyTime = readyTime;
    }

    public double getDueTime() {
        return dueTime;
    }

    public void setDueTime(double dueTime) {
        this.dueTime = dueTime;
    }

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }
}
