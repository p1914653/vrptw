package structure;


/**
 * Vehicule représente une section du graphe total, une tournée
 */
public class Vehicle {
    private Route route;
    private int charge;
    private double time;
    private int maxCharge;

    /**
     * Default constructor, please prefer the other one, with the deposit as parameter
     */
    public Vehicle() {
        this.charge = 0;
        this.time = 0;
        this.route = new Route();
    }
    public Vehicle(Client deposit, int maxCharge) {
        this.charge = 0;
        this.time = deposit.getReadyTime();
        this.route = new Route(deposit);
        this.maxCharge = maxCharge;
    }

    public int getMaxCharge() {
        return maxCharge;
    }

    public void setMaxCharge(int maxCharge) {
        this.maxCharge = maxCharge;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    /**
     * A function that adds a client to the route, checking if it is possible
     * @param client the client to add
     */
    public boolean addClient(Client client) {
        if(this.route.getPathing().isEmpty()){
            this.route.addClient(client);
            this.route.addTime(client, this.time);
            return true;
        }
        double duration = this.route.distance(this.route.getLastClient(), client);
        int totalCharge = this.charge + client.getDemand();
        if(this.time + duration > client.getDueTime() ||
                totalCharge>maxCharge ||
                this.time +
                duration +
                client.getService() +
                this.route.distance(this.route.getFirstClient(), client) > this.route.getFirstClient().getDueTime()) {
            return false;
        }
        else{
            this.charge=totalCharge;
            if(this.time + duration < client.getReadyTime()){
                this.time = client.getReadyTime()+ client.getService();
            }
            else {
                this.time += duration + client.getService();
            } //travel time + delivery time
            this.route.addClient(client);
            this.route.addTime(client, this.time);
            return true;
        }
    }

    /**
     * A function that removes the last client from the route
     */
    public void removeLast(){
        Client lastClient = this.route.getLastClient();
        this.route.removeLast();
        this.charge -= lastClient.getDemand();
        this.time = this.route.getTime(lastClient);
    }

    /**
     * A function that closes the route
     */
    public void closeRoute(){
        Client deposit = this.route.getFirstClient();
        this.route.addClient(deposit);
        double duration = this.route.distance(this.route.getLastClient(), deposit);
        this.time += duration;
    }

    /**
     *A function that prints the route on the standard output
     */
    public void print() {
        System.out.println("Charge: " + this.charge);
        System.out.println("Length: " + this.route.getPathLength());
        route.print();
    }

    /**
     * A function that checks if two vehicles are equal
     * @param vehicle the vehicle to compare to
     * @return true if the vehicles are equal, false otherwise
     */
    public boolean equals(Vehicle vehicle) {
    	if(!this.route.equals(vehicle.getRoute())) {
    		return false;
    	}
    	return true;
    }
}
