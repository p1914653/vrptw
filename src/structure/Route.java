package structure;

import java.util.HashMap;
import java.util.LinkedList;

import static java.lang.Math.*;

public class Route {
    /**
     * list of all the clients the vehicle has been visiting so far
     */
    private LinkedList<Client> pathing;
    /**
     * length of the path
     */
    private double pathLength;
    private HashMap<Integer, Double> timeMap;

    public Route() {
        this.pathing = new LinkedList<>();
        this.pathLength = 0;
        this.timeMap = new HashMap<>();
    }
    public Route(Client deposit) {
        this.pathing = new LinkedList<>();
        this.timeMap = new HashMap<>();
        this.pathLength = 0;
        this.pathing.add(deposit);
        this.addTime(deposit, deposit.getReadyTime());
    }

    public LinkedList<Client> getPathing() {
        return pathing;
    }

    /**
     * A function that returns the client at a given index in the pathing list
     * @param index the index of the client
     * @return
     */
    public Client getClient(int index) {
        return pathing.get(index);
    }

    public double getPathLength() {
        recalculatePathLength();
        return pathLength + distance(this.pathing.getLast(), this.pathing.getFirst()); //au cas où le chemin n'est pas fermé
    }

    public void setPathLength(double pathLength) {
        this.pathLength = pathLength;
    }

    public double getTime(Client client) {
        return timeMap.get(client.getIdName());
    }
    /**
     * A function that calculates the distance between two clients
     * @param client1 first client
     * @param client2 second client
     */
    public double distance(Client client1, Client client2){
        double dist=pow(client1.getX()-client2.getX(), 2)+pow(client1.getY()-client2.getY(), 2);
        dist = sqrt(dist);
        return dist;
    }

    /**
     * A function that adds a client to the pathing list
     * @param client the targetted client
     */
    public double addClient(Client client){
        if(this.pathing.isEmpty()){
            this.pathing.add(client);
            return 0;
        }
        double dist = distance(this.pathing.getLast(), client);
        this.pathing.add(client);
        this.setPathLength(this.getPathLength()+dist);
        return dist;
    }
    /**
     * A function that returns the last client in the pathing list
     */
    public Client getLastClient() {
        return this.pathing.getLast();
    }
    /**
     * A function that checks if the pathing list contains a client
     */
    public boolean contains(Client client) {
        return this.pathing.contains(client);
    }

    /**
     * A function that recalculates the path length
     */
    public void recalculatePathLength() {
        double pathLength = 0;
        for(int i = 0; i < this.pathing.size()-1; i++) {
            pathLength += this.distance(this.pathing.get(i), this.pathing.get(i+1));
        }
        this.setPathLength(pathLength);
    }

    /**
     * A function that adds the time as an entry in the timeMap
     * @param client the client to add or change
     * @param time the time when the vehicle leaves the client
     */
    public void addTime(Client client, double time) {
        this.timeMap.put(client.getIdName(), time);
    }

    /**
     * A function that removes the last client from the pathing list
     */
    public void removeLast() {
        this.timeMap.remove(this.pathing.getLast().getIdName());
        this.pathing.removeLast();
        this.recalculatePathLength();
    }

    /**
     * A function that returns the first client in the pathing list
     * @return the first client in the pathing list
     */
    public Client getFirstClient() {
        return this.pathing.getFirst();
    }

    /**
     * A function that prints the pathing list
     */
    public void print(){
        System.out.print("[ ");
        for(int i = 0; i < this.pathing.size(); i++) {
            Client client = this.pathing.get(i);
            System.out.print(client.getIdName() + " - ");
        }
        System.out.println("]");
    }

    /**
     * A function that checks if two routes are equal
     * @param route the route to compare to
     * @return true if the routes are equal, false otherwise
     */
    public boolean equals(Route route){
        if(this.pathing.size() != route.pathing.size()) {
            return false;
        }
        this.recalculatePathLength();
        route.recalculatePathLength();
        if(this.getPathLength() != route.getPathLength()) {
            return false;
        }
        for(int i = 1; i < this.pathing.size(); i++) {
            if(this.pathing.get(i).getIdName() != route.pathing.get(i).getIdName()) {
                return false;
            }
        }
        return true;
    }

}
