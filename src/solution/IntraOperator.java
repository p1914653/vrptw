package solution;

import structure.Client;
import structure.Route;
import structure.Vehicle;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that will calculate a neighbor of the stocked vehicle
 * this vehicle can then be reinserted in a neighbor
 */
public class IntraOperator {

    Vehicle vehicle;
    /**
     * Default constructor, not recommended
     */
    public IntraOperator(){
        vehicle = new Vehicle();
    }

    /**
     * Recommended constructor
     * @param vehicle_ vehicle to copy
     */
    public IntraOperator(Vehicle vehicle_){
        this.vehicle = new Vehicle();
        this.vehicle.setCharge(vehicle_.getCharge());
        this.vehicle.setTime(vehicle_.getTime());
        Route route = new Route();
        for(int i = 0; i < vehicle_.getRoute().getPathing().size(); i++){
            route.getPathing().add(vehicle_.getRoute().getPathing().get(i));
        }
        this.vehicle.setRoute(route);
        this.vehicle.setMaxCharge(vehicle_.getMaxCharge());
    }

    /**
     * get the vehicle stocked in the operator
     * @return the vehicle stocked in the operator
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * A function that swaps two clients in the route
     * @param i index of the first client
     * @param j index of the second client
     * @return the updated vehicle
     */
    public Vehicle exchange(int i, int j){
        Client clienti =this.vehicle.getRoute().getPathing().get(i);
        Client clientj =this.vehicle.getRoute().getPathing().get(j);
        boolean possible = true;
        Route oldRoute = this.vehicle.getRoute();
        Vehicle newVehicle = new Vehicle(oldRoute.getPathing().getFirst(), this.vehicle.getMaxCharge());
        for(int k = 1; k < oldRoute.getPathing().size()-1; k++){
            if(k == i){
                if(!newVehicle.addClient(clientj)) {
                    possible = false;
                    break;
                }
            }
            else if(k == j){
                if(!newVehicle.addClient(clienti)) {
                    possible = false;
                    break;
                }
            }
            else{
                if(!newVehicle.addClient(oldRoute.getPathing().get(k))) {
                    possible = false;
                    break;
                }
            }
        }
        if(possible) {
            newVehicle.closeRoute();
            return newVehicle;
        }
        else
            return null;
    }

    /**
     * A function that relocates a client in the route
     * @param i index of the client to relocate
     * @param j index of the new position of the client
     * @return the updated vehicle
     */
    public Vehicle relocate(int i, int j) {
        Client clienti = this.vehicle.getRoute().getPathing().get(i);
        Route oldRoute = this.vehicle.getRoute();
        if(j != i){
            Vehicle newVehicle = new Vehicle(oldRoute.getPathing().getFirst(), this.vehicle.getMaxCharge());
            boolean possible = true;
            for(int k = 1; k < oldRoute.getPathing().size()-1; k++){
                if(k == j){
                    if(!newVehicle.addClient(clienti)) {
                        possible = false;
                        break;
                    }
                }
                else {
                    if(j < i){
                        if(k< j || k > i){
                            if(!newVehicle.addClient(oldRoute.getPathing().get(k))) {
                                possible = false;
                                break;
                            }
                        }
                        else if(k > j && k<= i){
                            if(!newVehicle.addClient(oldRoute.getPathing().get(k-1))) {
                                possible = false;
                                break;
                            }
                        }
                    }
                    if (j > i) {
                        if(k< i || k>j){
                            if(!newVehicle.addClient(oldRoute.getPathing().get(k))) {
                                possible = false;
                                break;
                            }
                        }
                        if(k >= i && k < j){
                            if(!newVehicle.addClient(oldRoute.getPathing().get(k+1))) {
                                possible = false;
                                break;
                            }
                        }
                    }
                }
            }
            if(possible) {
                newVehicle.closeRoute();
                return newVehicle;
            }
        }
        return null;
    }


    /**
     * A function that returns the whole list of intra Neighbors of the vehicle
     * @return the list of intra Neighbors
     */

    public List<Vehicle> getIntraNeighborhood() {
        ArrayList<Vehicle> vehicles = new ArrayList<>();
        for(int i = 1; i < this.vehicle.getRoute().getPathing().size()-1; i++) {
            for(int j = i+1; j < this.vehicle.getRoute().getPathing().size()-1; j++) {
                if(j != i){
                    Vehicle ex = exchange(i, j);
                    if(ex != null) {
                        vehicles.add(ex);
                    }
                    if(j != i+1) {
                        Vehicle re = relocate(i, j);
                        if (re != null) {
                            vehicles.add(re);
                        }
                    }
                }
            }
        }
        return vehicles;
    }

}
