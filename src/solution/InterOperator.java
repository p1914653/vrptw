package solution;

import structure.Client;
import structure.Route;
import structure.Vehicle;

import java.util.ArrayList;
import java.util.List;
/**
 * Class that will calculate a neighbor of a stocked solution
 */
public class InterOperator {
    Neighbor vehicles;

    /**
     * Default constructor, not recommended
     */
    public InterOperator(){
        vehicles = new Neighbor();
    }

    /**
     * Recommended constructor
     * @param solution solution to copy
     */
    public InterOperator(Neighbor solution){
        this.vehicles = new Neighbor(solution.getVehicles());
    }

    /**
     * get the neighbor stocked in the operator
     * @return the neighbor stocked in the operator
     */
    public Neighbor getVehicles(){
        return vehicles;
    }

    /**
     * 2-opt operation which exchanges two clients between two vehicles
     * @param vehiclei index of the first vehicle
     * @param vehiclej index of the second vehicle
     * @param i index of the client in the first vehicle
     * @param j index of the client in the second vehicle
     * @return the updated neighbor
     */
    public Neighbor exchange(int vehiclei, int vehiclej, int i, int j){
        Client clienti = this.vehicles.get(vehiclei).getRoute().getPathing().get(i);
        Client clientj = this.vehicles.get(vehiclej).getRoute().getPathing().get(j);
        Route oldRoutei = this.vehicles.get(vehiclei).getRoute();
        Route oldRoutej = this.vehicles.get(vehiclej).getRoute();
        Vehicle newVehiclei = new Vehicle(oldRoutei.getPathing().getFirst(), this.vehicles.get(vehiclei).getMaxCharge());
        Vehicle newVehiclej = new Vehicle(oldRoutej.getPathing().getFirst(), this.vehicles.get(vehiclej).getMaxCharge());
        Neighbor neighbor = new Neighbor(this.vehicles.getVehicles());
        for(int k =1; k < oldRoutei.getPathing().size()-1; k++){
            if(k == i) {
                if (!newVehiclei.addClient(clientj)) {
                    return null;
                }
            }
            else if(!newVehiclei.addClient(oldRoutei.getPathing().get(k))){
                return null;
            }
        }
        for(int k=1; k < oldRoutej.getPathing().size()-1; k++){
            if(k == j) {
                if (!newVehiclej.addClient(clienti)) {
                    return null;
                }
            }
            else if(!newVehiclej.addClient(oldRoutej.getPathing().get(k))){
                return null;
            }
        }
        newVehiclei.closeRoute();
        newVehiclej.closeRoute();
        neighbor.setVehicle(vehiclei, newVehiclei);
        neighbor.setVehicle(vehiclej, newVehiclej);
        return neighbor;
    }

    /**
     * relocate operation which moves a client from one vehicle to another
     * @param vehiclei index of the initial vehicle
     * @param vehiclej index of the final vehicle
     * @param i index of the client in the first vehicle
     * @param j index of the client in the second vehicle
     * @return the updated neighbor
     */
    public Neighbor relocate(int vehiclei, int vehiclej, int i, int j){
        Client clienti = this.vehicles.get(vehiclei).getRoute().getPathing().get(i);
        Route oldRoutei = this.vehicles.get(vehiclei).getRoute();
        Route oldRoutej = this.vehicles.get(vehiclej).getRoute();
        Vehicle newVehiclei = new Vehicle(oldRoutei.getPathing().getFirst(), this.vehicles.get(vehiclei).getMaxCharge());
        Vehicle newVehiclej = new Vehicle(oldRoutej.getPathing().getFirst(), this.vehicles.get(vehiclej).getMaxCharge());
        Neighbor neighbor = new Neighbor(this.vehicles.getVehicles());
        for(int k =1; k < oldRoutei.getPathing().size()-1; k++) {
            if (k != i) {
                if (!newVehiclei.addClient(oldRoutei.getPathing().get(k))) {
                    return null;
                }
            }
        }
        for(int k=1; k < oldRoutej.getPathing().size()-1; k++) {
            if (k == j) {
                if (!newVehiclej.addClient(clienti)) {
                    return null;
                }
                if (!newVehiclej.addClient(oldRoutej.getPathing().get(k))) {
                    return null;
                }
            } else{
                if (!newVehiclej.addClient(oldRoutej.getPathing().get(k))) {
                    return null;
                }
            }
        }
        if(j == oldRoutej.getPathing().size()-1){
            if(!newVehiclej.addClient(clienti)){
                return null;
            }
        }
        newVehiclei.closeRoute();
        newVehiclej.closeRoute();
        neighbor.setVehicle(vehiclei, newVehiclei);
        neighbor.setVehicle(vehiclej, newVehiclej);
        return neighbor;
    }

    /**
     * operation which exchanges two portions of two vehicles
     * @param vehiclei the index of the first vehicle
     * @param vehiclej the index of the second vehicle
     * @param i the index of the first client to be relocated in vehicle i
     * @param j the index of the second client to be relocated in vehicle j
     * @return
     */
    public Neighbor crossExchange(int vehiclei, int vehiclej, int i, int j){
        Client clienti = this.vehicles.get(vehiclei).getRoute().getPathing().get(i);
        Client clientj = this.vehicles.get(vehiclej).getRoute().getPathing().get(j);
        boolean possible = true;
        Route oldRoutei = this.vehicles.get(vehiclei).getRoute();
        Route oldRoutej = this.vehicles.get(vehiclej).getRoute();
        Vehicle newVehiclei = new Vehicle(oldRoutei.getPathing().getFirst(), this.vehicles.get(vehiclei).getMaxCharge());
        Vehicle newVehiclej = new Vehicle(oldRoutej.getPathing().getFirst(), this.vehicles.get(vehiclej).getMaxCharge());
        Neighbor neighbor = new Neighbor(this.vehicles.getVehicles());
        if(oldRoutej.getPathing().size() ==2){
            for(int k =1; k < i; k++){
                newVehiclei.addClient(oldRoutei.getPathing().get(k));
            }
            for(int k=i; k< oldRoutei.getPathing().size()-1; k++){
                newVehiclej.addClient(oldRoutei.getPathing().get(k));
            }
            newVehiclei.closeRoute();
            newVehiclej.closeRoute();
            neighbor.setVehicle(vehiclei, newVehiclei);
            neighbor.setVehicle(vehiclej, newVehiclej);
            return neighbor;
        }
        for(int k =1; k < i; k++){
            if (!newVehiclei.addClient(oldRoutei.getPathing().get(k))) {
                return null;
            }
        }
        for(int k=j; k < oldRoutej.getPathing().size()-1; k++){
            if (!newVehiclei.addClient(oldRoutej.getPathing().get(k))) {
                return null;
            }
        }
        for(int k =1; k < j; k++){
            if (!newVehiclej.addClient(oldRoutej.getPathing().get(k))) {
                return null;
            }
        }
        for(int k=i; k < oldRoutei.getPathing().size()-1; k++){
            if (!newVehiclej.addClient(oldRoutei.getPathing().get(k))) {
                return null;
            }
        }
        newVehiclei.closeRoute();
        newVehiclej.closeRoute();
        neighbor.setVehicle(vehiclei, newVehiclei);
        neighbor.setVehicle(vehiclej, newVehiclej);
        return neighbor;
    }

    /**
     * generates the whole inter-neighborhood of the solution stocked in the operator
     * @return the neighborhood of the solution stocked in the operator
     */
    public List<Neighbor> getInterNeighborhood(){
        ArrayList<Neighbor> neighborhood = new ArrayList<>();
        //génération des voisins via exchange
        for(int i = 0; i < this.vehicles.getVehicles().size()-1; i++){ // itération sur le premier véhicule
            for(int j = i+1; j <this.vehicles.getVehicles().size(); j++){ // itération sur le second véhicule
                for(int k = 1; k < this.vehicles.get(i).getRoute().getPathing().size()-1; k++){ // itération sur le premier client
                    for(int l = 1; l < this.vehicles.get(j).getRoute().getPathing().size()-1; l++){ // itération sur le second client
                        Neighbor neighbor = this.exchange(i, j, k, l);
                        if(neighbor != null){
                            neighborhood.add(neighbor);
                        }
                    }
                }
            }
        }
        //génération des voisins via relocate
        for(int i = 0; i < this.vehicles.getVehicles().size(); i++){ // itération sur le premier véhicule
            for(int j = i+1; j <this.vehicles.getVehicles().size(); j++){ // itération sur le second véhicule
                for(int k = 1; k < this.vehicles.get(i).getRoute().getPathing().size()-1; k++){ // itération sur le premier client
                    for(int l = 1; l < this.vehicles.get(j).getRoute().getPathing().size(); l++){ // itération sur le second client (on laisse la possibilté de placer à l'id size-1 car cela veut juste dire qu'on le place à la fin du trajet)
                        Neighbor neighbor = this.relocate(i, j, k, l);
                        if (neighbor != null) {
                            neighborhood.add(neighbor);
                        }
                    }
                }
            }
        }
        //génération des voisins via crossExchange
        for(int i = 0; i< this.vehicles.getVehicles().size(); i++){
            for(int j = i+1; j< this.vehicles.getVehicles().size(); j++){
                for(int k = 2; k < this.vehicles.get(i).getRoute().getPathing().size()-1; k++){
                    for(int l = 2; l < this.vehicles.get(j).getRoute().getPathing().size()-1; l++){
                        if(!(k == this.vehicles.get(i).getRoute().getPathing().size()-1 && l == this.vehicles.get(j).getRoute().getPathing().size()-1)){
                            Neighbor neighbor = this.crossExchange(i, j, k, l);
                            if (neighbor != null) {
                                neighborhood.add(neighbor);
                            }
                        }
                    }
                }
            }
        }

        //génération d'un voisin avec un nouveau véhicule
        Vehicle newVehicle = new Vehicle(this.vehicles.get(0).getRoute().getFirstClient(), this.vehicles.get(0).getMaxCharge());
        newVehicle.closeRoute();
        this.vehicles.addVehicle(newVehicle);
        for(int i = 0; i< this.vehicles.getVehicles().size()-2; i++){
            for(int k = 1; k < this.vehicles.get(i).getRoute().getPathing().size()-1; k++){
                if(this.vehicles.get(i).getRoute().getPathing().size() > 3) {
                    Neighbor neighbor = this.relocate(i, this.vehicles.getVehicles().size() - 1, k, 1);
                    if (neighbor != null) {
                        neighborhood.add(neighbor);
                    }
                    if (k > 1 && k < this.vehicles.get(i).getRoute().getPathing().size() - 1) {
                        Neighbor neighbor2 = this.crossExchange(i, this.vehicles.getVehicles().size()-1, k, 1);
                        if (neighbor2 != null) {
                            neighborhood.add(neighbor2);
                        }
                    }
                }
            }
        }
        return neighborhood;
    }
}
