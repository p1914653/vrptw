package solution;

import java.math.BigDecimal;
import java.math.RoundingMode;
import data.Data;
import graph.GraphicDisplay;
import structure.*;

import java.util.*;

import static java.lang.Math.abs;
import static java.lang.Math.log;

public class SolutionGenerator {
    private Data data;
    private Neighbor solution;

    /**
     * Recommended constructor
     */
    public SolutionGenerator(Data data) {
        this.data = data;
        this.solution = new Neighbor(new ArrayList<Vehicle>());
    }

    /**
     * naive solution generator, adds clients one by one to the first vehicle that can take them
     */
    public void generateNaiveSolution() {
        LinkedList<Client> tempClients = (LinkedList<Client>) data.getClients().clone();
        while (!tempClients.isEmpty()) {
            int i =0;
            Vehicle currentVehicle = new Vehicle(data.getDeposit(), data.getMaxQuantity());
            solution.addVehicle(currentVehicle);
            Route currentRoute = currentVehicle.getRoute();
            while(i < tempClients.size()){
                Client client = tempClients.get(i);
                if (currentVehicle.addClient(client)) {
                    tempClients.remove(client);
                    i--;
                }
                i++;
            }
            currentVehicle.closeRoute();
        }
    }

    /**
     * random solution generator, adds clients one by one to a random vehicle that can take them
     */
    public void generateRandomSolution() {
        LinkedList<Client> tempClients = (LinkedList<Client>) data.getClients().clone();
        Random random = new Random();
        while (!tempClients.isEmpty()) {
            Vehicle currentVehicle = new Vehicle(data.getDeposit(), data.getMaxQuantity());
            solution.addVehicle(currentVehicle);
            Route currentRoute = currentVehicle.getRoute();
            int i =0;
            while(i < tempClients.size()) {
                Client client = tempClients.get(random.nextInt(tempClients.size()));
                if (currentVehicle.addClient(client)) {
                    tempClients.remove(client);
                    i--;
                }
                i++;
            }
            currentVehicle.closeRoute();
        }
    }

    public Neighbor getNeighbor() {
        return solution;
    }

    public List<Neighbor> getAllIntraNeighbors(){
        List<Neighbor> neighborhoods = new ArrayList<>();
        int i =0;
        for(Vehicle vehicle : solution.getVehicles()){
            if(vehicle.getRoute().getPathing().size() > 3){
                IntraOperator intraOperator = new IntraOperator(vehicle);
                List<Vehicle> localVar = intraOperator.getIntraNeighborhood();
                for(Vehicle v : localVar){
                    ArrayList<Vehicle> temp = new ArrayList<>(solution.getVehicles());
                    temp.set(i,v);
                    neighborhoods.add(new Neighbor(temp));
                }
            }
            i++;
        }
        return neighborhoods;
    }

    public List<Neighbor> getAllInterNeighbors(){
        InterOperator interOperator = new InterOperator(solution);
        List<Neighbor> neighborhoods =  interOperator.getInterNeighborhood();
        return neighborhoods;
    }

    public List<Neighbor> getAllNeighbors(){
        List<Neighbor> neighborhoods = new ArrayList<>();
        neighborhoods.addAll(getAllIntraNeighbors());
        neighborhoods.addAll(getAllInterNeighbors());
        for(Neighbor neighbor : neighborhoods){
            neighbor.delEmptyVehicles();
            neighbor.recalculateFitness();//nettoyage des solutions et recalcul de la fitness pour etre sûr que tout est juste
        }
        return neighborhoods;
    }

    public void reallyDumbOptimization(){//goes straight to the best neighbor
        ArrayList<Neighbor> neighborhoods;
        double fitness;
        double newFitness;
        do{
            neighborhoods = new ArrayList<>(getAllNeighbors());
            fitness = this.solution.getFitness();
            newFitness = fitness;
            for (Neighbor neighbor : neighborhoods) {
                if (neighbor.getFitness() < newFitness) {
                    this.solution = neighbor;
                    newFitness = neighbor.getFitness();
                }
            }
            System.out.println("new neighbor as a solution! fitness = " + this.solution.getFitness() +" nb of vehicles = " + this.solution.getVehicles().size());
        } while(newFitness != fitness);
    }

    public void simulatedAnnealing(double temperature, double decreaseFactor, int moves_per_temperature, boolean enableGraphicDisplay) {
        GraphicDisplay dsp = null;
        if (enableGraphicDisplay) {
            dsp = new GraphicDisplay();
            dsp.displayFromSolution(this.solution, true);
        }
        Random random = new Random();
        ArrayList<Neighbor> neighborhood;

        Neighbor currentNeighbor = this.solution;
        Neighbor bestEverNeighbor = this.solution;
        double bestEverFitness = bestEverNeighbor.getFitness();

        System.out.println("Début de recuit simulé. Fitness actuelle : " + this.round(bestEverFitness));

        boolean regenerate = false;
        neighborhood = new ArrayList<>(getAllNeighbors());

        while (temperature > 0.01) {
            for (int i = 0; i < moves_per_temperature; i++) {
                if (regenerate) {
                    neighborhood = new ArrayList<>(getAllNeighbors());
                    regenerate = false;
                }
                int rd = random.nextInt(neighborhood.size());
                Neighbor randomlyPickedNeighbor = neighborhood.get(rd);
                double differentielFitness = randomlyPickedNeighbor.getFitness() - currentNeighbor.getFitness();

                if (differentielFitness <= 0) {
                    currentNeighbor = randomlyPickedNeighbor;
                    this.solution = currentNeighbor;
                    if (currentNeighbor.getFitness() < bestEverFitness) {
                        bestEverNeighbor = currentNeighbor;
                        bestEverFitness = currentNeighbor.getFitness();
                        if (enableGraphicDisplay) dsp.refresh(solution);
                    }
                    regenerate = true;
                } else {
                    double p = random.nextDouble();
                    double limit = Math.exp(-differentielFitness / temperature);
                    if (p <= limit) {
                        currentNeighbor = randomlyPickedNeighbor;
                        this.solution = currentNeighbor;
                        regenerate = true;
                    }
                }
            }
            temperature *= decreaseFactor;
            System.out.println("(SA) Temp : " + this.round(temperature) + ". Fitness : " + this.round(this.solution.getFitness()) + ". Nb of vehicles = " + this.solution.getVehicles().size());

        }
        System.out.println("Recuit simulé terminé, fitness obtenue : " + this.round(bestEverFitness));
        System.out.println("Nombre de véhicules obtenu = " + bestEverNeighbor.getVehicles().size());
        this.solution = bestEverNeighbor;
    }

    public List<Neighbor> getNeighborsTabu(List<String> Tabulist, List<String> operationList){
        List<Neighbor> neighborhoods = new ArrayList<>();
        operationList.clear();
        for(Vehicle vehicle : this.solution.getVehicles()){
            if(vehicle.getRoute().getPathing().size() > 3){
                IntraOperator intraOperator = new IntraOperator(vehicle);
                for(int i = 1; i < vehicle.getRoute().getPathing().size()-1; i++) {
                    for(int j = i+1; j < vehicle.getRoute().getPathing().size()-1; j++) {
                        if(j != i){
                            Vehicle ex = intraOperator.exchange(i, j);
                            if(ex != null && !Tabulist.contains("intraexchange " + i + " " + j)) {
                                ArrayList<Vehicle> temp = new ArrayList<>(solution.getVehicles());
                                temp.set(this.solution.getVehicles().indexOf(vehicle),ex);
                                neighborhoods.add(new Neighbor(temp));
                                operationList.add("intraexchange " + vehicle.getRoute().getClient(i).getIdName() + " " + vehicle.getRoute().getClient(j).getIdName());
                            }
                            if(j != i+1) {
                                Vehicle re = intraOperator.relocate(i, j);
                                if (re != null && !Tabulist.contains("intrarelocate " + i + " " + j)) {
                                    ArrayList<Vehicle> temp = new ArrayList<>(solution.getVehicles());
                                    temp.set(this.solution.getVehicles().indexOf(vehicle),re);
                                    neighborhoods.add(new Neighbor(temp));
                                    operationList.add("intrarelocate " + vehicle.getRoute().getClient(i).getIdName() + " " + vehicle.getRoute().getClient(j).getIdName());
                                }
                            }
                        }
                    }
                }
            }
        }
        InterOperator interOperator = new InterOperator(solution);
        //génération des voisins via exchange
        for(int i = 0; i < this.solution.getVehicles().size()-1; i++){ // itération sur le premier véhicule
            for(int j = i+1; j <this.solution.getVehicles().size(); j++){ // itération sur le second véhicule
                for(int k = 1; k < this.solution.get(i).getRoute().getPathing().size()-1; k++){ // itération sur le premier client
                    for(int l = 1; l < this.solution.get(j).getRoute().getPathing().size()-1; l++){ // itération sur le second client
                        Neighbor neighbor = interOperator.exchange(i, j, k, l);
                        if(neighbor != null && !Tabulist.contains("interexchange " + this.solution.get(i).getRoute().getClient(k).getIdName() + " " + this.solution.get(j).getRoute().getClient(l).getIdName())){
                            neighborhoods.add(neighbor);
                            operationList.add("interexchange " + this.solution.get(i).getRoute().getClient(k).getIdName() + " " + this.solution.get(j).getRoute().getClient(l).getIdName());
                        }
                    }
                }
            }
        }
        //génération des voisins via relocate
        for(int i = 0; i < this.solution.getVehicles().size(); i++){ // itération sur le premier véhicule
            for(int j = i+1; j <this.solution.getVehicles().size(); j++){ // itération sur le second véhicule
                for(int k = 1; k < this.solution.get(i).getRoute().getPathing().size()-1; k++){ // itération sur le premier client
                    for(int l = 1; l < this.solution.get(j).getRoute().getPathing().size(); l++){ // itération sur le second client (on laisse la possibilté de placer à l'id size-1 car cela veut juste dire qu'on le place à la fin du trajet)
                        Neighbor neighbor = interOperator.relocate(i, j, k, l);
                        if (neighbor != null && !Tabulist.contains("interrelocate "+ this.solution.get(i).getRoute().getClient(k).getIdName() + " " + this.solution.get(j).getRoute().getClient(l).getIdName())){
                            neighborhoods.add(neighbor);
                            operationList.add("interrelocate " + this.solution.get(i).getRoute().getClient(k).getIdName() + " " + this.solution.get(j).getRoute().getClient(l).getIdName());
                        }
                    }
                }
            }
        }
        //génération des voisins via crossExchange
        for(int i = 0; i< this.solution.getVehicles().size(); i++){
            for(int j = i+1; j< this.solution.getVehicles().size(); j++){
                for(int k = 2; k < this.solution.get(i).getRoute().getPathing().size()-1; k++){
                    for(int l = 2; l < this.solution.get(j).getRoute().getPathing().size()-1; l++){
                        if(!(k == this.solution.get(i).getRoute().getPathing().size()-1 && l == this.solution.get(j).getRoute().getPathing().size()-1)){
                            Neighbor neighbor = interOperator.crossExchange(i, j, k, l);
                            if (neighbor != null && !Tabulist.contains("intercrossexchange " + this.solution.get(i).getRoute().getClient(k).getIdName() + " " + this.solution.get(j).getRoute().getClient(l).getIdName())){
                                neighborhoods.add(neighbor);
                                operationList.add("intercrossexchange " + this.solution.get(i).getRoute().getClient(k).getIdName() + " " + this.solution.get(j).getRoute().getClient(l).getIdName());
                            }
                        }
                    }
                }
            }
        }
        for(Neighbor neighbor : neighborhoods){
            neighbor.delEmptyVehicles();
            neighbor.recalculateFitness();//nettoyage des solutions et recalcul de la fitness pour etre sûr que tout est juste
        }
        return neighborhoods;
    }

    public void tabuSearch(int nbIterations, int tabuListSize, boolean enableGraphicDisplay){
        GraphicDisplay disp = new GraphicDisplay();
        if (enableGraphicDisplay) disp.displayFromSolution(this.solution, true);
        ArrayList<Neighbor> neighborhood;
        ArrayList<String> tabuList = new ArrayList<>();
        ArrayList<String> operationList = new ArrayList<>();
        double newFitness;
        boolean changed;
        double minimum;
        int minindex;
        Neighbor bestEver = this.solution;
        for(int i =0; i < nbIterations; i++){
            neighborhood = new ArrayList<>(getNeighborsTabu(tabuList, operationList));
            newFitness = this.solution.getFitness();
            changed = false;
            minimum = Double.MAX_VALUE;
            minindex = -1;
            for(Neighbor neighbor : neighborhood){
                if(neighbor.getFitness() != this.solution.getFitness()) {//on ignore les voisins qui ont la meme fitness, il est peu probable qu'ils soient réellement différents
                    if (neighbor.getFitness() < minimum) {
                        minimum = neighbor.getFitness();
                        minindex = neighborhood.indexOf(neighbor);
                    }
                    if (neighbor.getFitness() < newFitness) {
                        this.solution = neighbor;
                        newFitness = neighbor.getFitness();
                        changed = true;
                    }
                }
            }
            if(!changed && minindex == -1){
                System.out.println("no neighbor found at iteration " + i + " (neighborhood size = " + neighborhood.size() + ")");
                this.solution = bestEver;
                System.out.println("best solution found : " + this.solution.getFitness() + " nb of vehicles = " + this.solution.getVehicles().size());
                if (enableGraphicDisplay) disp.refresh(this.solution);
                return;
            }
            if(!changed){
                this.solution = neighborhood.get(minindex);
                tabuList.add(operationList.get(minindex));
            }
            newFitness = this.solution.getFitness();
            if(tabuList.size() > tabuListSize){
                tabuList.remove(0);
            }
            if(newFitness < bestEver.getFitness()){
                bestEver = this.solution;
            }
            System.out.println("(Tabu) Fitness : " + this.round(this.solution.getFitness()) +". Nb of vehicles = " + this.solution.getVehicles().size());
            if (enableGraphicDisplay) disp.refresh(this.solution);
        }
        this.solution = bestEver;
        System.out.println("Tabu Search terminé, fitness obtenue : " + this.round(this.solution.getFitness()));
        System.out.println("Nombre de véhicules obtenu = " + this.solution.getVehicles().size());
        if (enableGraphicDisplay) disp.refresh(this.solution);
    }

    /**
     * returns the ideal temperature for the simulated annealing algorithm
     * @param probability the probability of accepting a worse solution
     * @return
     */
    public double idealTemperature(double probability){
        ArrayList<Neighbor> neighborhood;
        neighborhood = new ArrayList<>(getAllNeighbors());
        double delta = 0;
        for(Neighbor neighbor : neighborhood){
            if(delta > abs(neighbor.getFitness() - this.solution.getFitness())) {
                delta = abs(neighbor.getFitness() - this.solution.getFitness());
            }
        }
        return -delta/log(probability);
    }

    private double round(double value) {
        return BigDecimal.valueOf(value).setScale(3, RoundingMode.HALF_UP).doubleValue();
    }


}
