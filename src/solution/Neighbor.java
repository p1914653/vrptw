package solution;

import structure.Vehicle;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a solution.
 * A solution is a list of vehicles, and a fitness is added to this list to represent the quality of the solution
 * The fitness is the sum of the path length of each vehicle
 * The fitness is calculated when the solution is created, and can be recalculated if needed
 */
public class Neighbor {
    ArrayList<Vehicle> vehicles;
    double fitness;

    public Neighbor(){
        vehicles = new ArrayList<>();
        fitness = 0;
    }

    public Neighbor(List<Vehicle> _vehicles){
        vehicles = new ArrayList<>(_vehicles);
        fitness = 0;
        for (Vehicle vehicle : vehicles) {
            fitness += vehicle.getRoute().getPathLength();
        }
    }

    public ArrayList<Vehicle> getVehicles(){
        return vehicles;
    }

    public double getFitness(){
        recalculateFitness();
        return fitness;
    }

    public void recalculateFitness(){
        fitness = 0;
        for (Vehicle vehicle : vehicles) {
            fitness += vehicle.getRoute().getPathLength();
        }
    }

    public void setFitness(double fitness){
        this.fitness = fitness;
    }

    public void setVehicles(ArrayList<Vehicle> vehicles){
        this.vehicles = new ArrayList<>(vehicles);
    }

    public boolean addVehicle(Vehicle vehicle){
        return this.vehicles.add(vehicle);
    }


    public void setVehicle(int index, Vehicle vehicle){
        vehicles.set(index, vehicle);
    }

    public void delEmptyVehicles(){
        for(int i = 0; i < vehicles.size(); i++){
            if(vehicles.get(i).getRoute().getPathing().size() == 2){
                vehicles.remove(i);
                i--;
            }
        }
    }

    /**
     * prints the summary of the current solution on the standard output
     */
    public void print_summary(){
        System.out.println("====================================");
        System.out.println("Summary:");
        System.out.println("Number of vehicles: " + vehicles.size());
        System.out.print("Total length: ");
        System.out.println(fitness);
        System.out.println("====================================");
    }



    /**
     * prints the current solution on the standard output
     */
    public void print(String solution_index){
        System.out.println("====================================");
        System.out.println("Solution " + solution_index + ":");
        for(Vehicle vehicle : vehicles){
            System.out.println("--------------------");
            System.out.print("Véhicule " + vehicles.indexOf(vehicle) + ": ");
            vehicle.print();
            System.out.println("--------------------");
        }
        System.out.println("====================================");
    }

    /**
     * prints the current solution on the standard output
     */
    public void print(){
        this.print("");
    }

    public Vehicle get(int index){
        return vehicles.get(index);
    }

    /**
     * Returns the number of vehicles in the solution (helpful with taboo search for example)
     * @param neighbor the neighbor to compare
     * @return true if the two neighbors are equal, false otherwise
     */
    public boolean equals(Neighbor neighbor){
        if(neighbor.getFitness() != this.getFitness()){
            return false;
        }
        if(neighbor.getVehicles().size() != this.getVehicles().size()){
            return false;
        }
        for(int i = 0; i < neighbor.getVehicles().size(); i++){
            if(!neighbor.get(i).equals(this.get(i))){
                return false;
            }
        }
        return true;
    }

    public boolean containedIn(ArrayList<Neighbor> neighbors){
        for(Neighbor neighbor : neighbors){
            if(this.equals(neighbor)){
                return true;
            }
        }
        return false;
    }
}
