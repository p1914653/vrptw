package graph;

import data.Data;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import solution.Neighbor;
import solution.SolutionGenerator;
import structure.Client;
import structure.Route;
import structure.Vehicle;

import java.util.*;


public class GraphicDisplay {
    private final static List<String> myColors = Arrays.asList("#000000","#00FF00","#0000FF","#FF0000","#01FFFE","#FFA6FE","#FFDB66","#006401","#010067","#95003A","#007DB5","#FF00F6","#FFEEE8","#774D00","#90FB92","#0076FF","#D5FF00","#FF937E","#6A826C","#FF029D","#FE8900","#7A4782","#7E2DD2","#85A900","#FF0056","#A42400","#00AE7E","#683D3B","#BDC6FF","#263400","#BDD393","#00B917","#9E008E","#001544","#C28C9F","#FF74A3","#01D0FF","#004754","#E56FFE","#788231","#0E4CA1","#91D0CB","#BE9970","#968AE8","#BB8800","#43002C","#DEFF74","#00FFC6","#FFE502","#620E00","#008F9C","#98FF52","#7544B1","#B500FF","#00FF78","#FF6E41","#005F39","#6B6882","#5FAD4E","#A75740","#A5FFD2","#FFB167","#009BFF","#E85EBE");
    private Graph graph;
    private Viewer viewer;

    public GraphicDisplay() {
        this.graph = new SingleGraph("Trajet VRPTW");
        this.graph.setAttribute("ui.quality");
        this.graph.setAttribute("ui.antialias");
        this.graph.setStrict(false);
    }

    public void displayFromSolution(Neighbor solution, Boolean displayLabels) {
        // Ajout du deposit
        Client deposit = solution.getVehicles().get(0).getRoute().getClient(0);
        addNode("0", deposit.getX(), deposit.getY(), "red", false);
        int cmp = 1;
        // Ajout des arêtes et des clients à la fois
        for (Vehicle vehicle : solution.getVehicles()) {
            LinkedList<Client> vehiclePathing = vehicle.getRoute().getPathing();
            Client client = new Client();
            Client nextClient = new Client();
            String randomColor = generateColor(); //Solution mise en place si on veut générer des couleurs aléatoirement (pb : des fois elles se ressemblent trop)


            for(int i = 0; i < vehiclePathing.size()-1; i++) {
                if(vehiclePathing.size() == 2) break;
                if(vehiclePathing.size() == 3 && i == 1) break;
                client = vehiclePathing.get(i);
                nextClient = vehiclePathing.get(i+1);
                if (nextClient.getIdName() != 0) {
                    addNode(Integer.toString(nextClient.getIdName()), nextClient.getX(), nextClient.getY(), "none", displayLabels);
                }
                addEdge(Integer.toString(client.getIdName()), Integer.toString(nextClient.getIdName()), myColors.get(cmp));
            }
            cmp++;
        }

        //Visualisation du graphe
        this.viewer = this.graph.display();
        this.viewer.getDefaultView();
        this.viewer.disableAutoLayout();

    }

    public void refresh(Neighbor solution) {
        graph.getEdgeSet().clear();
        // Ajout des arêtes et des clients à la fois
        int cmp = 0;
        for (Vehicle vehicle : solution.getVehicles()) {
            LinkedList<Client> vehiclePathing = vehicle.getRoute().getPathing();
            Client client = new Client();
            Client nextClient = new Client();

            for(int i = 0; i < vehiclePathing.size()-1; i++) {
                if(vehiclePathing.size() == 2) break;
                if(vehiclePathing.size() == 3 && i == 1) break;
                client = vehiclePathing.get(i);
                nextClient = vehiclePathing.get(i+1);
                addEdge(Integer.toString(client.getIdName()), Integer.toString(nextClient.getIdName()), myColors.get(cmp));
            }
            cmp++;
        }
    }


    private void addNode(String name, int posX, int posY, String color, Boolean displayLabels) {
        Node node = this.graph.addNode(name);
        node.setAttribute("xy", posX, posY);
        if (!color.equals("none")) node.addAttribute("ui.style", "fill-color:" + color + ";");
        if (displayLabels) node.setAttribute("ui.label", "c" + name);
    }

    private void addEdge(String startingNodeName, String endingNodeName, String color) {
        Edge edge = this.graph.addEdge(startingNodeName + "-" + endingNodeName, startingNodeName, endingNodeName);
        if (!color.equals("none")) edge.setAttribute("ui.style", "fill-color:" + color + ";");
    }

    private String generateColor() {
        Random random = new Random();
        return "rgb(" + random.nextInt(256) + "," + random.nextInt(256) + "," + random.nextInt(256) + ")";
    }

}
